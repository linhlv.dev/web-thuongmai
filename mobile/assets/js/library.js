//Menu Mobile-----------------
var $menu = $("#mainMenu").clone();
$menu.attr("id", "my-mobile-menu");
$menu.mmenu({
    extensions: ["theme-white"]
});


//fix header
var section = $("#fixed-header");
var start = $(section).offset().top;
// var main = $("#main");
$.event.add(window, "scroll", function() {
    var b = $(window).scrollTop();
    $(section).css("position", ((b) > start) ? "fixed" : "relative");
    $(section).css("top", ((b) > start) ? "0px" : "");
    $(section).css("width", ((b) > start) ? "100%" : "");
    // $(main).css("margin-top", ((b) > start) ? "112px" : "");
    if (b <= 30) {
        $(section).removeClass("scrollHeader")
    } else {
        $(section).addClass("scrollHeader")
    }
})


//click menu footer
if ($(window).width() <= 650) {
    $('.footer-top .col-footer').find('.title-footer').click(function(){
        $(this).next('.list-footer').toggle();
    })
}







 // tabbed content
    // http://www.entheosweb.com/tutorials/css/tabs.asp
    $(".tab_content").hide();
    $(".tab_content:first").show();

  /* if in tab mode */
    $("ul.tabs li").click(function() {
		
      $(".tab_content").hide();
      var activeTab = $(this).attr("rel"); 
      $("#"+activeTab).fadeIn();		
		
      $("ul.tabs li").removeClass("active");
      $(this).addClass("active");

	  $(".tab_drawer_heading").removeClass("d_active");
	  $(".tab_drawer_heading[rel^='"+activeTab+"']").addClass("d_active");
	  
    });
	/* if in drawer mode */
	$(".tab_drawer_heading").click(function() {
      
      $(".tab_content").hide();
      var d_activeTab = $(this).attr("rel"); 
      $("#"+d_activeTab).fadeIn();
	  
	  $(".tab_drawer_heading").removeClass("d_active");
      $(this).addClass("d_active");
	  
	  $("ul.tabs li").removeClass("active");
	  $("ul.tabs li[rel^='"+d_activeTab+"']").addClass("active");
    });
	
	
	/* Extra class "tab_last" 
	   to add border to right side
	   of last tab */
	$('ul.tabs li').last().addClass("tab_last");